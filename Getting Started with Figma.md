<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Getting Started with Figma</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__html"><h1 id="getting-started-with-figma">Getting Started with Figma</h1>
<p>Welcome to <strong>Retail Operations</strong>! It’s great to see you here!</p>
<p>Guessing that your onboarding is still ongoing, we will try to help you with <a href="https://www.figma.com/">Figma</a>, the design tool that Retail Operations Product Design Team (a.k.a ROX) uses in order to create seamless experiences.</p>
<p>Needless to say, you won’t be learning every part of the tool, that’s the designer’s job. Instead, you will learn what Figma is about and how you can use it to check a design document, see the code (if you are a developer) or the associated component from the design system.</p>
<p>Now grab a coffee, sit back and enjoy. <a href="https://open.spotify.com/playlist/37i9dQZF1DWZwtERXCS82H?si=pbBZzKjAQb64-jNIGeDSPw">Here is a playlist for you</a> if you prefer listening to music while learning new stuff.</p>
<blockquote>
<p>This document is not designed for designers, who -hopefully- know how to use a design tool. Of course, we won’t restrain designers from reading it if they find it interesting.</p>
</blockquote>
<h2 id="why-do-we-use-figma">Why do we use Figma?</h2>
<p>Until 2018, ROX was using <a href="https://www.sketch.com/">Sketch</a> as the main design tool. Then they felt the need to change their design stack and made the switch to Figma. There are various reasons for that and if you are interested with the details, <a href="https://docs.google.com/presentation/d/1veZMn5zyvpy4bfoqz-3DNrY1EbK2BtLa9uQjsyobmO8/edit?usp=sharing">you can find the elaborate explanation by checking this deck</a>.</p>
<p>To give you a quick summary, these are our favorite features from Figma:</p>
<ul>
<li>It is an all-in-one design tool that faciliates design, prototype, developer hand-off and version history.</li>
<li>It is 100% cloud-based. Nothing is saved locally.</li>
<li>Figma is platform agnostic. Whatever operating system you are using, if you have a modern web browser, Figma will work seamlessly with it.</li>
<li>The collaboration features are excellent. Multiple people can work on the same design file at the same time.</li>
<li>Developer hand-off is a breeze.</li>
</ul>
<blockquote>
<p>If you are a geek and want to learn the ins and outs of the tool, consider <a href="https://www.youtube.com/channel/UCQsVmhSa4X-G3lHlUtejzLA/playlists">checking Figma YouTube channel</a>. There are great tutorial videos that you might find useful.</p>
</blockquote>
<h2 id="how-do-i-log-in-to-figma">How do I log in to Figma?</h2>
<p>Figma is free to use to view and export design files. So there is a 99.9% chance that you won’t need a license to use it for your purposes.</p>
<p><img src="https://i.imgur.com/AhkGJUj.png" alt="Figma login page"></p>
<ul>
<li>Go to <a href="https://www.figma.com/login">Figma login page</a>.</li>
<li>Click on “Log in with Google”.</li>
<li>Proceed with your Zalando account.</li>
</ul>
<blockquote>
<p>If you are a designer and need to create designs with Figma, you can <a href="https://help.figma.com/article/347-upgrade-a-member-of-your-organziation#request">request an upgrade for your role</a>. Note that an admin from Zalando will need to review your request.</p>
</blockquote>
<h1 id="accessing-files">Accessing files</h1>
<p>Your log in is successful, that means now you are ready to rock’n’roll with Figma! First of all, wait for your favorite designer to share a file with you.</p>
<p>When a new file is shared with you, you will be notified via email.</p>
<p><img src="https://i.imgur.com/dgadIkS.png" alt="Email notification for new Figma file"></p>
<p>Click on “Open in Figma” button. That’s it, you’re in!</p>
<h1 id="viewing-the-design">Viewing the design</h1>
<p>Figma follows the following hiearchy:</p>
<pre><code>File &gt; Pages &gt; Frames &gt; Design Components
</code></pre>
<p>It means that a design file may include several <em>pages</em>, which may include several <em>frames</em>, which most probably include <em>design components</em>.</p>
<p>Here is an example Figma file to understand where to find each of these elements:</p>
<p><img src="https://i.imgur.com/tdYa8Kn.jpg" alt="Example Figma file"></p>
<h3 id="pages">Pages</h3>
<p>In the <em>Pages</em> section, you will find all the pages (duh!) created by the designer. Ideally, designers should name their pages for you to understand what each page is about.</p>
<blockquote>
<p>How to spot a lazy designer? If their pages are called Page 1, Page 2 etc., we have a lazy designer on the board!</p>
</blockquote>
<h3 id="frames">Frames</h3>
<p><em>Frames</em> (some people can call them <em>Artboards</em> as well) are part of <em>Pages</em>. They group design elements in a meaningful way. Ideally, designers should name their frames for you to understand what each frame is about.</p>
<blockquote>
<p>How to spot a lazy designer? If their frames are called Frame 1, Frame 2 etc., we have a lazy designer on the board!</p>
</blockquote>
<h3 id="design-components">Design Components</h3>
<p>Design components are part of frames, which are part of pages. A design component can be a vector, shape, image, text etc.</p>
<p>Ideally, we don’t design components from scratch, but use <a href="https://fabric-next.retail-operations-test.zalan.do/">Fabric</a>, which is our beloved component library, so that the design experience is coherent across every tool. So, expect the designer to use out-of-the-box components from Fabric.</p>
<blockquote>
<p>How to spot a lazy designer? If their layers are not grouped and renamed, then you can make sure that we have a lazy designer on the board!</p>
</blockquote>
<h1 id="exporting-components-as-an-image">Exporting components as an image</h1>
<p>Let’s say you are a Product Specialist and you want to include a part of the design to your ticket. Or you are a front-end developer and you want to export an image.</p>
<p>Instead of taking a screenshot, you can choose the component you want to have and export it.</p>
<p>To do that, select the component from the Frames area or click on it directly. Once it is selected, find the “Export” panel on the right and click on <strong>+</strong>.</p>
<p><img src="https://i.imgur.com/W2ywqUF.png" alt="Exporting a component as an image"></p>
<p>Choose the image format and click on the export button. That’s it!</p>
<blockquote>
<p>You can export the whole frame too. To do that, simply select the frame from the left panel. While it is selected, find the “Export” panel on the right and proceed.</p>
</blockquote>
<h1 id="using-the-code-panel">Using the Code Panel</h1>
<p>If you are a front-end developer, you will find the code panel pretty handy. From the code panel, you can:</p>
<ul>
<li>View properties of selected layers or components,</li>
<li>Measure the distances between objects,</li>
<li>More importantly, you can understand which component is used from Fabric.</li>
</ul>
<blockquote>
<p>To learn more about how to view properties of selected layers and measure the distances, you can refer to <a href="https://help.figma.com/article/32-developer-handoff#code">the related section in the Figma documentation</a>.</p>
</blockquote>
<h3 id="understanding-which-component-is-used-from-fabric">Understanding which component is used from Fabric</h3>
<p>The more you will be exposed to design files, the more you will get the hang of it and understand Fabric components better.</p>
<p>Remember: The best way to make sure about the components is to <strong>sit down with the designer and quickly align</strong> on it! Designers will be glad to support you on that.</p>
<p>But of course you can understand more by simply looking at the code from the code panel.</p>
<p>Let’s say you clicked on a component and saw this code:</p>
<pre><code>/*  master/tabs/medium/text-only  */

position: static;
left: 0%;
right: 0%;
top: 0%;
bottom: 0%;
</code></pre>
<p>The very first line of the code, which is <em><strong>master/tabs/medium/text-only</strong></em> will give you the hint that the designer used the medium sized Tabs component from Fabric.</p>
<p>But again, the best way to make sure is talking to designers. Don’t be shy, they are nice people!</p>
<h1 id="exporting-screenshots">Exporting a frame as an image</h1>
<p>In order to export a frame as an image directly from Figma, you need to:</p>
<ol>
<li>Select the frame that you want to export <img src="https://imgur.com/JHvkyoi.jpg" alt="Selection of component or frame in Figma"></li>
<li>Activate the export panel in Figma and choose the destination of your image file <img src="https://imgur.com/r8yYfm9.jpg" alt="Exprt of selected component in Figma"></li>
</ol>

<h1 id="what-if-i-have-further-questions">What if I have further questions?</h1>
<p>There is a good chance that the team you are working with has already an embedded designer, they should be sitting nearby. Go and ask them anything.</p>
<p>Or ask your question via ROX team’s email: <a href="mailto:RetailOperations-UX@zalando.de">RetailOperations-UX@zalando.de</a></p>
<p>Take care, peace out! 🖖</p>
</div>
</body>

</html>
